/******************************************************************************/
/*                                                                            */
/*   Furgalladas                                                              */
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   printerSupervisor.ino - 3d printer supervisor project


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> printerSupervisor.ino          </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 26-November-2018 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> printerSupervisor </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2018] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef I_PRINTERSUPERVISOR_CPP
#define I_PRINTERSUPERVISOR_CPP


//Main library
#include "supervisor.h"


//LCD variables (LCD TYPE: 16x2, cell 5x8)
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>  // F Malpartida's NewLiquidCrystal library

//Temp/humidity sensor DHT11
#include <DHT.h>
DHT dhtObject(TEMP_HUM_PIN, DHTTYPE);

//Check pin definitions in supervisor.h

//Declare printer supervisor
Supervisor printerSupervisor(BUZZER_PIN,CONFIG_BUTTON_PIN,UP_BUTTON_PIN,DOWN_BUTTON_PIN,SELECT_BUTTON_PIN,RELAY_PIN,ENDSTOP_PIN,TEMP_HUM_PIN,FJSENSOR_PIN);
//Define auxiliar function/objects
LiquidCrystal_I2C  lcdObject(I2C_ADDR,En_pin,Rw_pin,Rs_pin,D4_pin,D5_pin,D6_pin,D7_pin);


void setup(){
  Serial.begin(9600);
  //Register all auxiliar objects in supervisorpyh
  printerSupervisor.registerLCD(&lcdObject);
  printerSupervisor.registerDHT(&dhtObject);
  //Endstop interrupt
  attachInterrupt(digitalPinToInterrupt(ENDSTOP_PIN), endstopInterrupt, FALLING); //define external interrupts   
  //Init Status
  printerSupervisor.printLCDMessage(ONE_SHOT,WELCOME_MESSAGE, NO_VALUE);
  printerSupervisor.startTemporizer();
}

void loop(){
  uint8_t remainingValue;
  delay(200);
  switch (printerSupervisor.getStateMachineStatus()){ 
    case INIT:
      if (printerSupervisor.checkTemporizer(INIT_TEMPORIZER,remainingValue)){
        printerSupervisor.setStateMachineStatus(WAIT_TO_START);
        printerSupervisor.printLCDMessage(ONE_SHOT,START_MESSAGE, NO_VALUE);
        printerSupervisor.printLCDMessage(CONTINUOUS_MODE_TEMP_SMOKE,NO_VALUE, NO_VALUE);
        //Activate relay
        printerSupervisor.activateRelay();
      }
      if (printerSupervisor.isConfigPressed()||printerSupervisor.isSetPressed()){
        printerSupervisor.defaultPreferences();
        printerSupervisor.printLCDMessage(ONE_SHOT,DEFAULT_PREFERENCES, NO_VALUE);
      }
    break;
    case WAIT_TO_START:
      if (printerSupervisor.isSetPressed()){
        if (printerSupervisor.isUpPressed()){
          printerSupervisor.setStateMachineStatus(TRUSTME_MODE);
          printerSupervisor.printLCDMessage(ONE_SHOT,TRUSTME_MESSAGE, 0x01);
          printerSupervisor.startTrustmeCounter(TRUSTME_COUNTER);
        }else{
          printerSupervisor.setStateMachineStatus(WAIT_FINISH);
          printerSupervisor.printLCDMessage(ONE_SHOT,MONITOR_MESSAGE, NO_VALUE);
          printerSupervisor.startMonitoring();//Start monitoring!
        }
      }
      if (printerSupervisor.isConfigPressed()){
        printerSupervisor.setStateMachineStatus(CONFIG_MODE);
        printerSupervisor.printLCDMessage(CONTINUOUS_MODE_OFF,NO_VALUE, NO_VALUE);
        printerSupervisor.configPrintCurrentMode();
        printerSupervisor.FJSensorInitialize();
        delay(1000);
      }
      printerSupervisor.checkWarningAlarm();
    break;    
    case TRUSTME_MODE:
        if (printerSupervisor.checkTrustmeCounter()){
          printerSupervisor.setStateMachineStatus(WAIT_FINISH);
          printerSupervisor.printLCDMessage(ONE_SHOT,MONITOR_MESSAGE, NO_VALUE);
          printerSupervisor.startMonitoring();//Start monitoring!
          printerSupervisor.buzzerTone(BUZZER_TONE_1);
        }
        if (printerSupervisor.isDownPressed()){
          printerSupervisor.setStateMachineStatus(WAIT_TO_START);
          printerSupervisor.printLCDMessage(ONE_SHOT,START_MESSAGE, NO_VALUE);
          printerSupervisor.printLCDMessage(CONTINUOUS_MODE_TEMP_SMOKE,NO_VALUE, NO_VALUE);
        }
    break;    
    case WAIT_FINISH:
      if (printerSupervisor.isEndStopPressed()){
        printerSupervisor.setStateMachineStatus(FINISHED_PRINT);
        printerSupervisor.printLCDMessage(CONTINUOUS_MODE_OFF,NO_VALUE, NO_VALUE);
        printerSupervisor.printLCDMessage(ONE_SHOT,FINISHED_PRINT_MESSAGE, NO_VALUE);
        printerSupervisor.startTemporizer();
        printerSupervisor.activateBuzzer(BUZZER_MODE_1);
      }
      if (printerSupervisor.checkAlarms()){
        //Some alarm raised!!!
        printerSupervisor.setStateMachineStatus(EMERGENCY_ALARM);
        printerSupervisor.printLCDMessage(ONE_SHOT,EMERGENCY_ALARM_MESSAGE, NO_VALUE);
        printerSupervisor.activateBuzzer(BUZZER_MODE_2);
        printerSupervisor.startTemporizer();
      }
      if (printerSupervisor.isDownPressed()){
        printerSupervisor.setStateMachineStatus(WAIT_TO_START);
        printerSupervisor.printLCDMessage(ONE_SHOT,START_MESSAGE, NO_VALUE);
        printerSupervisor.printLCDMessage(CONTINUOUS_MODE_TEMP_SMOKE,NO_VALUE, NO_VALUE);
      }
    break;
    case FINISHED_PRINT:
      if(printerSupervisor.checkTemporizer(FINISHED_TEMPORIZER,remainingValue)){
        printerSupervisor.setStateMachineStatus(FINISHED);
        printerSupervisor.printLCDMessage(ONE_SHOT,FINISHED_MESSAGE,NO_VALUE);
        //Deactivate relay
        printerSupervisor.deactivateRelay();
        //Deactivate buzzer
        printerSupervisor.deactivateBuzzer();
        
      }else{
        printerSupervisor.printLCDMessage(ONE_SHOT,FINISHED_TEMPORIZER_MESSAGE_VALUE,remainingValue);
        if (printerSupervisor.isSetPressed()){
          //Back to WAIT_FINISH state
          printerSupervisor.setStateMachineStatus(WAIT_FINISH);
          printerSupervisor.printLCDMessage(ONE_SHOT,MONITOR_MESSAGE, NO_VALUE);
          printerSupervisor.startMonitoring();//Start monitoring!
          printerSupervisor.deactivateBuzzer();
          printerSupervisor.printLCDMessage(CONTINUOUS_MODE_TEMP_SMOKE,NO_VALUE, NO_VALUE);
        }
      }
    break;
    case FINISHED:
      //Nothing to do
    break;
    case EMERGENCY_ALARM:
      if (printerSupervisor.checkTemporizer(EMERGENCY_TEMPORIZER,remainingValue)){
        printerSupervisor.setStateMachineStatus(EMERGENCY);
        printerSupervisor.printLCDMessage(CONTINUOUS_MODE_OFF,NO_VALUE, NO_VALUE);
        printerSupervisor.printLCDMessage(ONE_SHOT,EMERGENCY_MESSAGE,NO_VALUE);
        printerSupervisor.deactivateRelay();
      }else{
        printerSupervisor.printLCDMessage(ONE_SHOT,FINISHED_TEMPORIZER_MESSAGE_VALUE,remainingValue);
      }
      if (printerSupervisor.isSetPressed()){
        printerSupervisor.setStateMachineStatus(WAIT_TO_START);
        printerSupervisor.deactivateBuzzer();
        printerSupervisor.printLCDMessage(ONE_SHOT,START_MESSAGE, NO_VALUE);
        printerSupervisor.printLCDMessage(CONTINUOUS_MODE_TEMP_SMOKE,NO_VALUE, NO_VALUE);
        printerSupervisor.startMonitoring();//Start monitoring!
      }
    break;
    case EMERGENCY:
      //Nothing to do in this mode
    break;
    case CONFIG_MODE:
      if (printerSupervisor.isConfigPressed()){
        printerSupervisor.setStateMachineStatus(WAIT_TO_START);
        printerSupervisor.printLCDMessage(ONE_SHOT,START_MESSAGE, NO_VALUE);
        printerSupervisor.printLCDMessage(CONTINUOUS_MODE_TEMP_SMOKE,NO_VALUE, NO_VALUE);
      }
      if (printerSupervisor.isSetPressed()){
        printerSupervisor.configEditMode();
        printerSupervisor.setStateMachineStatus(CONFIG_EDIT_MODE);
      }
      if (printerSupervisor.isUpPressed()){
        printerSupervisor.configPrintNextMode();
      }
      if (printerSupervisor.isDownPressed()){
        printerSupervisor.configPrintPreviousMode();
      }
    break;
    case CONFIG_EDIT_MODE:
      if (printerSupervisor.isSetPressed()){
        printerSupervisor.setStateMachineStatus(CONFIG_MODE);
        printerSupervisor.configSaveConfig();
        printerSupervisor.configPrintCurrentMode();
      }
      if (printerSupervisor.isUpPressed()){
        printerSupervisor.configEditIncrementOption();
      }
      if (printerSupervisor.isDownPressed()){
        printerSupervisor.configEditDecrementOption();
      }
    break;
  }
  printerSupervisor.backgroundRoutine();
}

////ISR routine
void endstopInterrupt(){ //subprocedure for detecting end event
  if (printerSupervisor.endstopDebouncingRoutine()){
    printerSupervisor.setInterrupt();
  }
}

#endif // I_PRINTERSUPERVISOR_CPP
