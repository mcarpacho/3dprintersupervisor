/******************************************************************************/
/*                                                                            */
/*   Furgalladas                                                              */
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   supervisorConfiguration.h - 3d printer supervisor project


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> supervisorConfiguration.h                     </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 26-November-2018 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> printerSupervisor </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2018] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _SUPERVISORCONFIGURATION_H_
#define _SUPERVISORCONFIGURATION_H_


//Printer supervisor versioning
#define VERSION_MAJOR       1
#define VERSION_MINOR       0

//Pin definitions
#define ENDSTOP_PIN         2
#define BUZZER_PIN          3
#define CONFIG_BUTTON_PIN   4
#define UP_BUTTON_PIN       5
#define DOWN_BUTTON_PIN     6
#define SELECT_BUTTON_PIN   7
#define RELAY_PIN           8
#define TEMP_HUM_PIN        9
#define FJSENSOR_PIN        10
#define SMOKESENSOR_PIN     A0

//Button status
#define BUTTON_UNPRESSED    0
#define BUTTON_PRESSED      1

//Temperature / humidity sensor DHT11
#define DHTTYPE DHT11   // DHT 11

//State machine
#define INIT                0
#define WAIT_TO_START       1
#define WAIT_FINISH         2
#define FINISHED_PRINT      3
#define FINISHED            4
#define EMERGENCY_ALARM     5
#define EMERGENCY           6
#define CONFIG_MODE         7
#define CONFIG_EDIT_MODE    8
#define TRUSTME_MODE        9

//State definitions
#define ACTIVE              0
#define INACTIVE            1

//Alarm definitions
#define NO_ALARM            0
#define SMOKE               1
#define TEMPERATURE         2
#define FILAMENT_JAM        3

//Message type
#define WELCOME_MESSAGE                   0
#define START_MESSAGE                     1
#define PRINT_TEMP                        2
#define PRINT_HUM                         3 
#define MONITOR_MESSAGE                   4
#define FINISHED_PRINT_MESSAGE            5
#define FINISHED_MESSAGE                  6
#define FINISHED_TEMPORIZER               7
#define FINISHED_TEMPORIZER_MESSAGE_VALUE 8
#define EMERGENCY_ALARM_MESSAGE           9
#define EMERGENCY_MESSAGE                 10
#define DEFAULT_PREFERENCES               11
#define TRUSTME_MESSAGE                   12

//LCD modes
#define NO_VALUE                      0
#define ONE_SHOT                      1
#define CONTINUOUS_MODE_OFF           2
#define CONTINUOUS_MODE_TEMP          3
#define CONTINUOUS_MODE_TEMP_SMOKE    4

//LCD constants
#define LCD_SECONDS_TO_PRINT          5
#define LCD_NUMBER_PARAMS_TO_PRINT    3
#define MILLISECONDS_IN_SECOND        1000
#define LCD_UPDATE_TIME               1500

//Debouncing routine
#define DEBOUNCING_VALUE              600

//Trustme counter
#define TRUSTME_COUNTER               2

//Temporizer types
#define INIT_TEMPORIZER         0
#define FINISHED_TEMPORIZER     1
#define EMERGENCY_TEMPORIZER    2

//Temporizer values
#define INIT_TEMP_VALUE             3000 
#define FINISHED_TEMPORIZER_VALUE   15000 
#define EMERGENCY_TEMPORIZER_VALUE  15000 
#define WARNING_TEMPORIZER_VALUE    30000

//Buzzer continuous modes
#define BUZZER_OFF              0
#define BUZZER_MODE_1           1
#define BUZZER_MODE_2           2
#define BUZZER_MODE_3           3

//Buzzer oneShot tones
#define BUZZER_TONE_1           1

//FJSensor
#define FJSENSOR_OFF            0
#define FJSENSOR_ON             1

//Default thresholds
#define TEMP_DEFAULT_TH                    40
#define SMOKE_DEFAULT_TH                   500
#define FJSENSOR_ON_STABLISH_TIMEOUT       15000//Ok 
#define FJSENSOR_ONOFF_TIMEOUT             20000 //ok
#define FJSENSOR_OFF_TIMEOUT               100000 //Ok
#define FJSENSOR_OFF_STABLESTATE_TIMEOUT   5000 //Ok

//Config mode macros
#define MAX_CONFIG_MODES        6

//Config modes
#define CONFIG_TEMP_MODE            0
#define CONFIG_SMOKE_MODE           1
#define CONFIG_BUZZER_ACTIVATION    2
#define CONFIG_OFF_TIMEOUT          3
#define CONFIG_EMERGENCY_TIMEOUT    4
#define CONFIG_FJSENSOR             5
#define CONFIG_SAVE_EEPROM          6


//LCD defines
#define I2C_ADDR    0x3f  // Define I2C Address for the PCF8574T 
//---(Following are the PCF8574 pin assignments to LCD connections )----
// This are different than earlier/different I2C LCD displays
#define BACKLIGHT_PIN  3
#define En_pin  2
#define Rw_pin  1
#define Rs_pin  0
#define D4_pin  4
#define D5_pin  5
#define D6_pin  6
#define D7_pin  7
#define LED_OFF       1
#define LED_ON        0
#define LCD_ROWS       2
#define LCD_COLUMNS    16


#endif //  _SUPERVISORCONFIGURATION_H_
