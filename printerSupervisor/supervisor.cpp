/******************************************************************************/
/*                                                                            */
/*   Furgalladas                                                              */
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   supervisor.cpp - 3d printer supervisor project


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> printerSupervisor.cpp          </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 26-November-2018 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> printerSupervisor </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2018] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#ifndef _SUPERVISOR_C_
#define _SUPERVISOR_C_

#include "supervisor.h"

/* initialization code is done in the constructor, in supervisor.h, just with initialization list */
Supervisor::Supervisor(uint8_t inBuzzerPin, uint8_t inConfigButtonPin, uint8_t inUpButtonPin, uint8_t inDownButtonPin, 
  uint8_t inSetButtonPin, uint8_t inRelayPin, uint8_t inEndstopPin, uint8_t inTempSensorPin, uint8_t inFJSensorPin){
  //Pin configuration
  pinMode(inBuzzerPin, OUTPUT);
  pinMode(inConfigButtonPin, INPUT);
  pinMode(inUpButtonPin, INPUT);
  pinMode(inDownButtonPin, INPUT);
  pinMode(inSetButtonPin, INPUT);
  pinMode(inRelayPin, OUTPUT);
  pinMode(inEndstopPin, INPUT);
  pinMode(inFJSensorPin, INPUT);
  //Initialization
  supervisor.state=INIT;
  supervisor.alarm=NO_ALARM;
  supervisor.saveEEPROM=false;
  buzzer.buzzerActive=true;
  buzzer.buzzerFlag=false;
  fjSensor.previousState=FJSensorGetSensorValue();
  //LCD initialization
  lcdInternalVars.continuosMode=CONTINUOUS_MODE_OFF;
  //Pinout
  buzzer.buzzerPin=inBuzzerPin;
  fjSensor.FJSensorPin=inFJSensorPin;
  tempSensor.tempSensorPin=inTempSensorPin;
  buttonStatus.setButtonPin=inSetButtonPin;
  buttonStatus.configButtonPin=inConfigButtonPin;
  buttonStatus.upButtonPin=inUpButtonPin;
  buttonStatus.downButtonPin=inDownButtonPin;
  relay.relayPin=inRelayPin;
  analogWrite (buzzer.buzzerPin, 255);
  //Config mode
  configMode.currentMode=0;
  //EEPROM preferences
  loadEEPROMPreferences();
  //Debouncing timestamp
  debouncingTimestamp = millis();
}

///////////////////
//Buzzer handle
///////////////////

void Supervisor::activateBuzzer(uint8_t mode){
  buzzer.mode=mode;
}

void Supervisor::deactivateBuzzer(){
  analogWrite(buzzer.buzzerPin,255);
  buzzer.mode=BUZZER_OFF;
}

void Supervisor::checkWarningAlarm(){
  if (millis()-supervisor.temporizerMark>WARNING_TEMPORIZER_VALUE){
    buzzer.buzzerFlag = true;
    buzzer.mode=BUZZER_MODE_3;
  }
}

///////////////////
//State machine handle
///////////////////

uint8_t Supervisor::getStateMachineStatus(){
  return supervisor.state;
}

void Supervisor::setStateMachineStatus(uint8_t newState){
  supervisor.state=newState;
}


///////////////////
//Temporizer handle
///////////////////

void Supervisor::startMonitoring(){
  //Reset endstop state
  endStop.endStopState=BUTTON_UNPRESSED;
}

void Supervisor::startTemporizer(){
  supervisor.temporizerMark=millis();
}

boolean Supervisor::checkTemporizer(uint8_t temporizerType, uint8_t &remainingValue){  
  switch(temporizerType){
    case INIT_TEMPORIZER:
      if ((millis()-supervisor.temporizerMark)>INIT_TEMP_VALUE){        
        return true;
      }else{
        remainingValue=(INIT_TEMP_VALUE-(millis()-supervisor.temporizerMark))/1000;
        return false;
      }
    break;
    case FINISHED_TEMPORIZER:
      if ((millis()-supervisor.temporizerMark)>FINISHED_TEMPORIZER_VALUE){        
        return true;
      }else{
        remainingValue=(FINISHED_TEMPORIZER_VALUE-(millis()-supervisor.temporizerMark))/1000;
        return false;
      }
    break;
    case EMERGENCY_TEMPORIZER:
      if ((millis()-supervisor.temporizerMark)>supervisor.emergencyTimeout){        
        return true;
      }else{
        remainingValue=(supervisor.emergencyTimeout-(millis()-supervisor.temporizerMark))/1000;
        return false;
      }
    break;
  }
}

//////////////
//EndStop handle
//////////////

endStopType Supervisor::checkEndStop(){
  endStopType endStopOut;
  endStopOut.endStopState=digitalRead(endStop.endStopPin);
  return endStopOut;
}


//////////////
//Relay handle
//////////////

void Supervisor::activateRelay(){
  digitalWrite(relay.relayPin,LOW);
}

void Supervisor::deactivateRelay(){
  digitalWrite(relay.relayPin,HIGH);
}


///////////
//LCD handle
///////////

void Supervisor::registerLCD(LiquidCrystal_I2C*  lcdObject){
  lcd=lcdObject;
  initializeLCD();
}

void Supervisor::initializeLCD(){
  //Init lcd
  lcd->begin (LCD_COLUMNS,LCD_ROWS);  // initialize the lcd 
  // Switch on the backlight
  lcd->setBacklightPin(BACKLIGHT_PIN,POSITIVE);
  lcd->setBacklight(LED_ON);
  lcd->clear();
  delay(1000);
  lcd->home();   
  lcd->backlight();  
}

bool Supervisor::lcdNeedsUpdate(){
  if (millis()-lcdInternalVars.lcdUpdateMarkTime>LCD_UPDATE_TIME){
    return true;
  }
  return false;
}

void Supervisor::lcdResetUpdate(){
  lcdInternalVars.lcdUpdateMarkTime = millis();
}

void Supervisor::printLCDMessage(uint8_t mode, uint8_t messageType, uint8_t value){
  switch (mode){
    case ONE_SHOT:
      switch (messageType){
        case WELCOME_MESSAGE:
          lcd->setCursor(0,0);
          lcd->print("3D-Printer  ");
          lcd->setCursor(0,1);
          lcd->print("Supervisor  ");
          lcd->setCursor(12,1);
          lcd->print("v");
          lcd->setCursor(13,1);
          lcd->print(VERSION_MAJOR);
          lcd->setCursor(14,1);
          lcd->print(".");
          lcd->print(VERSION_MINOR);
        break;
        case START_MESSAGE: 
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("PRESS SET");
          lcd->setCursor(0,1);
          lcd->print("TO START");
        break; 
        case MONITOR_MESSAGE:
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("Monitor");
        break;
        case FINISHED_PRINT_MESSAGE: 
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("Print finished");
          lcd->setCursor(0,1);
          lcd->print("Ab:");
        break;
        case FINISHED_MESSAGE: 
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("Finished");
          lcd->setCursor(0,1);
          lcd->print("Turned Off");
        break;
        case FINISHED_TEMPORIZER_MESSAGE_VALUE: 
          lcd->setCursor(4,1);
          lcd->print("  ");
          lcd->setCursor(4,1);
          lcd->print(value);
        break;
        case EMERGENCY_ALARM_MESSAGE:
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("EMERGENCY!!!");
          lcd->setCursor(0,1);
          if (supervisor.alarm==SMOKE){
            lcd->print("SMOKE");
          }
          if (supervisor.alarm==TEMPERATURE){
            lcd->print("TEMPERATURE");
          } 
          if (supervisor.alarm==FILAMENT_JAM){
            lcd->print("FILAMENT JAM");
          }             
        break;
        case EMERGENCY_MESSAGE:
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("EMERGENCY!!!");
          lcd->setCursor(0,1);
          lcd->print("TURNED OFF");        
        break;
        case DEFAULT_PREFERENCES:
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("DEFAULT PREFER.");
          lcd->setCursor(0,1);
          lcd->print("LOADED");        
        break;
        case TRUSTME_MESSAGE:
          clearLCD();
          lcd->setCursor(0,0);
          lcd->print("Trustme");
          lcd->setCursor(0,1);
          lcd->print(value);
        break;
      }
    break;
    case CONTINUOUS_MODE_TEMP: 
      lcdInternalVars.continuosMode=CONTINUOUS_MODE_TEMP;
    break;
    case CONTINUOUS_MODE_TEMP_SMOKE:
      lcdInternalVars.continuosMode=CONTINUOUS_MODE_TEMP_SMOKE;
    break;
    case CONTINUOUS_MODE_OFF: 
      lcdInternalVars.continuosMode=CONTINUOUS_MODE_OFF;
    break;
  }
}


void Supervisor::clearLCD(){
  uint8_t i,j;
  for (i=0; i<17; i++){
    for (j=0; j<2; j++){
      lcd->setCursor(i,j);
      lcd->print(" ");
    }
  }
}


///////////////
//FJSensor access
///////////////

void Supervisor::FJSensorInitialize(){
  fjSensor.previousState=FJSensorGetSensorValue();
  fjSensor.lastFallingEdgeTimeMark=millis();
  fjSensor.lastFallingEdgeTimeMark=millis();
   fjSensor.lastLowStableStateTimeMark=millis();
}

uint8_t Supervisor::FJSensorGetSensorValue(){
  return digitalRead(fjSensor.FJSensorPin);
}

bool Supervisor::FJSensorCheckAlarm(){
  unsigned long tempMark;
  uint8_t currentState;
  bool alarmRaised;

  if (!supervisor.fjSensorActive){ //Sensor disabled => Exit with false!
    return false;
  }
  
  alarmRaised=false;
  tempMark=millis();
  currentState=FJSensorGetSensorValue();
  //First, check alarms

  
  if (fjSensor.previousState==LOW){//previous states were low
    if(currentState==LOW){//sensor continue with low value
      if (tempMark-fjSensor.lastFallingEdgeTimeMark>FJSENSOR_OFF_TIMEOUT){//check long low state jam detection
        alarmRaised=true;
      }
    }else{//New positive edge =>Check stablish timeout to debounce values
      fjSensor.lastRisingEdgeTimeMark=tempMark;
    }
    if (tempMark-fjSensor.lastFallingEdgeTimeMark>FJSENSOR_OFF_STABLESTATE_TIMEOUT){
      fjSensor.lastLowStableStateTimeMark=tempMark;
    }
  }

  if (fjSensor.previousState==HIGH){//previous states were low
    if(currentState==HIGH){//sensor continue with low value
      if (tempMark-fjSensor.lastRisingEdgeTimeMark>FJSENSOR_ON_STABLISH_TIMEOUT){//timeout accomplished long high state jam detection
        alarmRaised=true;
      }
    }else{//New falling edge 
      fjSensor.lastFallingEdgeTimeMark=tempMark;
    }
    //Check Jam2 condition ==>alternative on-off pulses
    if ((tempMark-fjSensor.lastFallingEdgeTimeMark<FJSENSOR_OFF_TIMEOUT)&&(tempMark-fjSensor.lastLowStableStateTimeMark>FJSENSOR_ONOFF_TIMEOUT)){
      alarmRaised=true;
    }
  }

  //Update previous state
  fjSensor.previousState=currentState;

  return alarmRaised;
}

///////////////
//Button access
///////////////

//Button status
buttonStatusType Supervisor::checkButtonStatus(){
  buttonStatusType buttonStatusOut;
  buttonStatusOut.setButtonStatus=digitalRead(buttonStatus.setButtonPin);
  buttonStatusOut.upButtonStatus=digitalRead(buttonStatus.upButtonPin);
  buttonStatusOut.downButtonStatus=digitalRead(buttonStatus.downButtonPin);
  buttonStatusOut.configButtonStatus=digitalRead(buttonStatus.configButtonPin);
  return buttonStatusOut;  
}

bool Supervisor::isSetPressed(){
  if (buttonStatus.setButtonStatus==BUTTON_PRESSED){
    return true; 
  }
  return false;
}

bool Supervisor::isConfigPressed(){
  if (buttonStatus.configButtonStatus==BUTTON_PRESSED){
    return true; 
  }
  return false;
}

bool Supervisor::isUpPressed(){
  if (buttonStatus.upButtonStatus==BUTTON_PRESSED){
    return true; 
  }
  return false;
}

bool Supervisor::isDownPressed(){
  if (buttonStatus.downButtonStatus==BUTTON_PRESSED){
    return true; 
  }
  return false;
}

//Endstop access 
bool Supervisor::isEndStopPressed(){
  if (endStop.endStopState==BUTTON_PRESSED){
    endStop.endStopState=BUTTON_UNPRESSED;
    return true; 
  }
  return false;
}

void Supervisor::registerDHT(DHT* dhtObject){
  dht=dhtObject;
  dht->begin();
}

//Check if some alarm was triggered
bool Supervisor::checkAlarms(){
  if (supervisor.alarm!=NO_ALARM){
    return true;
  }
  return false;
}

void Supervisor::setInterrupt(){
  endStop.endStopState=BUTTON_PRESSED;
  trustmeCounter--;
}

void Supervisor::startTrustmeCounter(uint8_t counterValue){
  trustmeCounter=counterValue;
}


boolean Supervisor::checkTrustmeCounter(void){
  if (trustmeCounter==0){
    return true;
  }
  return false;
}

boolean Supervisor::endstopDebouncingRoutine(void){
  if (millis()>debouncingTimestamp+DEBOUNCING_VALUE){
    debouncingTimestamp = millis();
    return true;
  }
  return false;
}


//////////////////
//  Config functions
//////////////////

void Supervisor::configPrintCurrentMode(){
  clearLCD();
  lcd->setCursor(0,0);
  switch(configMode.currentMode){
    case CONFIG_TEMP_MODE:
      lcd->print("Cfg Temp th");
    break;
    case CONFIG_SMOKE_MODE:
      lcd->print("Cfg Smoke th");
    break;
    case CONFIG_BUZZER_ACTIVATION:
      lcd->print("Cfg buzzer act");
    break;
    case CONFIG_OFF_TIMEOUT:
      lcd->print("Cfg off timeout");
    break;
    case CONFIG_EMERGENCY_TIMEOUT:
      lcd->print("Cfg timeout turnoff");
    break;
    case CONFIG_FJSENSOR:
      lcd->print("Cfg FJ Sensor");
    break;
    case CONFIG_SAVE_EEPROM:
      lcd->print("Save cfg to EEPROM");
    break;
  }
}
void Supervisor::configPrintNextMode(){
  configMode.currentMode++;
  if (configMode.currentMode>MAX_CONFIG_MODES){
    configMode.currentMode=0;
  }
  configPrintCurrentMode();
}
void Supervisor::configPrintPreviousMode(){
  if (configMode.currentMode==0){
    configMode.currentMode=MAX_CONFIG_MODES;
  }else{
    configMode.currentMode--;
  }
  configPrintCurrentMode();
}

void Supervisor::configEditMode(){
    lcd->setCursor(0,1);
  switch(configMode.currentMode){
    case CONFIG_TEMP_MODE:
      lcd->print("t:");
      lcd->setCursor(2,1);
      lcd->print((int)dht->readTemperature());
      lcd->setCursor(7,1);
      lcd->print("th:");
      lcd->setCursor(10,1);
      lcd->print("   ");
      lcd->setCursor(10,1);
      lcd->print(tempSensor.temperatureThreshold);
    break;
    case CONFIG_SMOKE_MODE:
      lcd->print("s:");
      lcd->setCursor(2,1);
      lcd->print(smokeSensor.value);
      lcd->setCursor(7,1);
      lcd->print("th:");
      lcd->setCursor(10,1);
      lcd->print("   ");
      lcd->setCursor(10,1);
      lcd->print(smokeSensor.threshold);
    break;
    case CONFIG_BUZZER_ACTIVATION:
      lcd->print("buzzer:");
      lcd->setCursor(7,1);
      if (buzzer.buzzerActive){
        lcd->print("ON ");
      }else{
        lcd->print("OFF");
      }
    break;
    case CONFIG_OFF_TIMEOUT:
      lcd->print("off timeout:");
      lcd->setCursor(12,1);
      lcd->print(supervisor.finishedTimeout);
    break;
    case CONFIG_EMERGENCY_TIMEOUT:
      lcd->print("em.Timeout:");
      lcd->setCursor(12,1);
      lcd->print(supervisor.emergencyTimeout);
    break;
    case CONFIG_FJSENSOR:
      lcd->print("fjSensor:");
      lcd->setCursor(7,1);
      if (supervisor.fjSensorActive){
        lcd->print("ON ");
      }else{
        lcd->print("OFF");
      }
    break;
    case CONFIG_SAVE_EEPROM:
      lcd->print("save:");
      lcd->setCursor(6,1);
      if (supervisor.saveEEPROM){
        lcd->print("yes");
      }else{
        lcd->print("no ");
      }
    break;
  }
}

void Supervisor::configEditIncrementOption(){
    lcd->setCursor(0,1);
  switch(configMode.currentMode){
    case CONFIG_TEMP_MODE:
      tempSensor.temperatureThreshold++;
    break;
    case CONFIG_SMOKE_MODE:
      smokeSensor.threshold=smokeSensor.threshold+10;
    break;
    case CONFIG_BUZZER_ACTIVATION:
      buzzer.buzzerActive=!buzzer.buzzerActive;
    break;
    case CONFIG_OFF_TIMEOUT:
      supervisor.finishedTimeout++;
    break;
    case CONFIG_EMERGENCY_TIMEOUT:
      supervisor.emergencyTimeout++;
    break;
    case CONFIG_FJSENSOR:
      supervisor.fjSensorActive=!supervisor.fjSensorActive;
    break;
    case CONFIG_SAVE_EEPROM:
      supervisor.saveEEPROM=!supervisor.saveEEPROM;
    break;
  }
  configEditMode();
}
void Supervisor::configEditDecrementOption(){
    lcd->setCursor(0,1);
  switch(configMode.currentMode){
    case CONFIG_TEMP_MODE:
      if (tempSensor.temperatureThreshold !=0){
        tempSensor.temperatureThreshold--;
      }
    break;
    case CONFIG_SMOKE_MODE:
      if (smokeSensor.threshold>=10){
        smokeSensor.threshold =smokeSensor.threshold-10;
      }
    break;
    case CONFIG_BUZZER_ACTIVATION:
      buzzer.buzzerActive=!buzzer.buzzerActive;
    break;
    case CONFIG_OFF_TIMEOUT:
      if (supervisor.finishedTimeout !=0){
        supervisor.finishedTimeout--;
      }
    break;
    case CONFIG_FJSENSOR:
      supervisor.fjSensorActive=!supervisor.fjSensorActive;
    break;    
    case CONFIG_EMERGENCY_TIMEOUT:
      if (supervisor.emergencyTimeout !=0){
        supervisor.emergencyTimeout--;
      }
    break;
    case CONFIG_SAVE_EEPROM:
      supervisor.saveEEPROM=!supervisor.saveEEPROM;
    break;
  }
  configEditMode();  
}

void Supervisor::configSaveConfig(){
  lcd->setCursor(0,1);
  lcd->print("Saved             ");  
  switch(configMode.currentMode){
    case CONFIG_TEMP_MODE:
      //Nothing to do
    case CONFIG_SMOKE_MODE:
    case CONFIG_BUZZER_ACTIVATION:
    case CONFIG_OFF_TIMEOUT:
    case CONFIG_EMERGENCY_TIMEOUT:
    break;
    case CONFIG_SAVE_EEPROM:
      storeEEPROMPreferences();
    break;
  }
  configEditMode();  
}


////////////////
//EEPROM handling
////////////////

void Supervisor::storeEEPROMPreferences(){
  EEPROMPreferences.finishedTimeout=supervisor.finishedTimeout;
  EEPROMPreferences.emergencyTimeout=supervisor.emergencyTimeout;
  EEPROMPreferences.buzzerActive=buzzer.buzzerActive;
  EEPROMPreferences.temperatureThreshold=tempSensor.temperatureThreshold;
  EEPROMPreferences.smokeThreshold=smokeSensor.threshold;
  EEPROMPreferences.fjSensorActive=supervisor.fjSensorActive;
  EEPROM_writePreferences(0, EEPROMPreferences);
}

void Supervisor::loadEEPROMPreferences(){
  EEPROM_readPreferences(0, EEPROMPreferences);
  supervisor.finishedTimeout=EEPROMPreferences.finishedTimeout;
  supervisor.emergencyTimeout=EEPROMPreferences.emergencyTimeout;
  buzzer.buzzerActive=EEPROMPreferences.buzzerActive;
  tempSensor.temperatureThreshold=EEPROMPreferences.temperatureThreshold;
  smokeSensor.threshold=EEPROMPreferences.smokeThreshold;
  supervisor.fjSensorActive=EEPROMPreferences.fjSensorActive;
}

void Supervisor::defaultPreferences(){
  supervisor.finishedTimeout=FINISHED_TEMPORIZER;
  supervisor.emergencyTimeout=EMERGENCY_TEMPORIZER_VALUE;
  buzzer.buzzerActive=true;
  tempSensor.temperatureThreshold=TEMP_DEFAULT_TH;
  smokeSensor.threshold=SMOKE_DEFAULT_TH;
  supervisor.fjSensorActive=FJSENSOR_OFF;
}

/////////////////////////
//Buzzer one shot routine
/////////////////////////
void Supervisor::buzzerTone(uint8_t buzzerType){
  switch(buzzerType){
    case BUZZER_TONE_1: //5 small beep in 1 second
      analogWrite (buzzer.buzzerPin, 0);
      delay(100);
      analogWrite (buzzer.buzzerPin, 255);
      delay(100);
      analogWrite (buzzer.buzzerPin, 0);
      delay(100);
      analogWrite (buzzer.buzzerPin, 255);
      delay(100);
      analogWrite (buzzer.buzzerPin, 0);
      delay(100);
      analogWrite (buzzer.buzzerPin, 255);
      delay(100);
      analogWrite (buzzer.buzzerPin, 0);
      delay(100);
      analogWrite (buzzer.buzzerPin, 255);
      delay(100);
      analogWrite (buzzer.buzzerPin, 0);
      delay(100);
      analogWrite (buzzer.buzzerPin, 255);                
    break;
  }
  
}
////////////////////
//Background routine
////////////////////

void Supervisor::backgroundRoutine(){
  uint16_t seconds, secondsMod, paramPrint;
  unsigned long tempMark;
  uint8_t fjSensorTempAlarm;
  //refresh button status
  if (digitalRead(buttonStatus.setButtonPin)==HIGH){
    buttonStatus.setButtonStatus=BUTTON_PRESSED;
  }else{
    buttonStatus.setButtonStatus=BUTTON_UNPRESSED;
  }
  if (digitalRead(buttonStatus.configButtonPin)==HIGH){
    buttonStatus.configButtonStatus=BUTTON_PRESSED;   
  }else{
    buttonStatus.configButtonStatus=BUTTON_UNPRESSED;
  }
  if (digitalRead(buttonStatus.upButtonPin)==HIGH){
    buttonStatus.upButtonStatus=BUTTON_PRESSED;
  }else{
    buttonStatus.upButtonStatus=BUTTON_UNPRESSED;
  }
  if (digitalRead(buttonStatus.downButtonPin)==HIGH){
    buttonStatus.downButtonStatus=BUTTON_PRESSED;
  }else{
    buttonStatus.downButtonStatus=BUTTON_UNPRESSED;
  }

  //Smoke sensor value 
  smokeSensor.value=analogRead(SMOKESENSOR_PIN);
  
  
  //Alarm: Temperature or smoke
  if ((dht->readTemperature()>tempSensor.temperatureThreshold)||(smokeSensor.value>smokeSensor.threshold)){
    if (dht->readTemperature()>tempSensor.temperatureThreshold){
      supervisor.alarm=TEMPERATURE;
    }else{
      supervisor.alarm=SMOKE;
    }
  }else{
    supervisor.alarm=NO_ALARM;
  }

  //Alarm: FJSensor monitoring
  if (FJSensorCheckAlarm()){
    supervisor.alarm=FILAMENT_JAM;
  }

  //LCD continuous monitoring
  if ((lcdInternalVars.continuosMode!=CONTINUOUS_MODE_OFF)&&(lcdNeedsUpdate())){
    lcdResetUpdate();
    switch(lcdInternalVars.continuosMode){      
      case CONTINUOUS_MODE_TEMP_SMOKE:
        seconds=millis()/MILLISECONDS_IN_SECOND;
        secondsMod=seconds%(LCD_SECONDS_TO_PRINT*LCD_NUMBER_PARAMS_TO_PRINT);
        paramPrint=secondsMod/LCD_SECONDS_TO_PRINT;
        switch(paramPrint){
          case 0:           
            lcd->setCursor(10,0);
            lcd->print("      ");
            lcd->setCursor(10,0);
            lcd->print("T:");
            lcd->setCursor(12,0);
            lcd->print((int)dht->readTemperature());
            //Print threshold
            lcd->setCursor(10,1);
            lcd->print("      ");
            lcd->setCursor(10,1);
            lcd->print("Tt");
            lcd->setCursor(12,1);
            lcd->print(tempSensor.temperatureThreshold);
          break;
          case 1: 
            lcd->setCursor(10,0);
            lcd->print("      ");
            lcd->setCursor(10,0);
            lcd->print("H:");
            lcd->setCursor(12,0);
            lcd->print((int)dht->readHumidity());
            //Clear threshold print
            lcd->setCursor(10,1);
            lcd->print("     ");
          break;
          case 2: 
            lcd->setCursor(10,0);
            lcd->print("      ");
            lcd->setCursor(10,0);
            lcd->print("S:");
            lcd->setCursor(12,0);
            lcd->print(smokeSensor.value);
            //Print threshold
            lcd->setCursor(10,1);
            lcd->print("St");
            lcd->setCursor(12,1);
            lcd->print(smokeSensor.threshold);
          break;
        }
      break;
    }
  }

  //Buzzer monitoring
  if (buzzer.buzzerActive){
    tempMark=millis();
    switch (buzzer.mode){
      case BUZZER_MODE_1: //Mode 1: 3 beeps in 1 second, and another second in silence      
        if (((tempMark/1000)%2)==0){
          /*if (((tempMark%1000)<200)||(((tempMark%1000)<600)&&((tempMark%1000)>=400))||(((tempMark%1000)<1000)&&((tempMark%1000)>=800))){
            analogWrite (buzzer.buzzerPin, 0);
          }else{
            analogWrite (buzzer.buzzerPin, 255);
          }*/
          analogWrite (buzzer.buzzerPin, 0);
          delay(200);
          analogWrite (buzzer.buzzerPin, 255);
          delay(150);
          analogWrite (buzzer.buzzerPin, 0);
          delay(200);
          analogWrite (buzzer.buzzerPin, 255);
          delay(150);
          analogWrite (buzzer.buzzerPin, 0);
          delay(200);
          analogWrite (buzzer.buzzerPin, 255);
          delay(150);
        }else{
          analogWrite (buzzer.buzzerPin, 255);
        }
      break;
      case BUZZER_MODE_2: //Mode 2: one second on, one second off
        if (((tempMark/1000)%2)==0){
          analogWrite (buzzer.buzzerPin, 255);
        }else{
          analogWrite (buzzer.buzzerPin, 0);
        }
      break;
      case BUZZER_MODE_3: //Mode 3: small beep
          analogWrite (buzzer.buzzerPin, 0);
          delay(100);
          analogWrite (buzzer.buzzerPin, 255);
          delay(100);
          analogWrite (buzzer.buzzerPin, 0);
          delay(100);
          analogWrite (buzzer.buzzerPin, 255);
          //buzzer.buzzerActive = false;
          buzzer.mode=BUZZER_OFF;
          this->startTemporizer();                 
      break;
    }
  }
}

#endif // _SUPERVISOR_C_
