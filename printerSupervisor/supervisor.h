/******************************************************************************/
/*                                                                            */
/*   Furgalladas                                                              */
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   supervisor.h - 3d printer supervisor project


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> supervisor.h                     </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 26-November-2018 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> printerSupervisor </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2018] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _SUPERVISOR_H_
#define _SUPERVISOR_H_


#include "Arduino.h"
#include <inttypes.h>
#include <avr/pgmspace.h>
#include "supervisorEEPROMStorage.h"
//Auxiliar libraries
#include <Wire.h>
#include <LCD.h>
#include <LiquidCrystal_I2C.h>
#include <DHT.h>
#include "supervisorTypes.h"
#include "supervisorConfiguration.h"

class Supervisor {
  public:
    //Constructor con lista de inicializadores de constructor
    Supervisor(uint8_t buzzerPin, uint8_t configButtonPin, uint8_t upButtonPin, uint8_t downButtonPin,
               uint8_t setButtonPin, uint8_t relayPin, uint8_t endstopPin, uint8_t tempSensorPin, uint8_t fjSensorPin);
    void activateBuzzer(uint8_t mode);
    void deactivateBuzzer();
    uint8_t getStateMachineStatus();
    void setStateMachineStatus(uint8_t newState);
    void registerLCD(LiquidCrystal_I2C*  lcd);
    void registerDHT(DHT* dht);
    void printLCDMessage(uint8_t mode, uint8_t messageType, uint8_t value);
    void setInterrupt();
    //Temporizers
    boolean checkTemporizer(uint8_t temporizerType, uint8_t &remainingValue);
    void startTemporizer();
    void activateRelay();
    void deactivateRelay();
    //fjSensor
    uint8_t FJSensorGetSensorValue();
    void FJSensorInitialize();
    bool FJSensorCheckAlarm();
    //buttons ports
    bool isSetPressed();
    bool isConfigPressed();
    bool isUpPressed();
    bool isDownPressed();
    //EndStop port
    bool isEndStopPressed();
    //Background routine
    void backgroundRoutine();
    //Start monitoring function
    void startMonitoring();
    //Check alarm trigger
    bool checkAlarms();
    //Config modes
    void configPrintCurrentMode();
    void configPrintNextMode();
    void configPrintPreviousMode();
    void configEditMode();
    void configEditIncrementOption();
    void configEditDecrementOption();
    void configSaveConfig();
    void defaultPreferences();
    void checkWarningAlarm();
    boolean endstopDebouncingRoutine(void);
    void startTrustmeCounter(uint8_t trustmeCounter);
    boolean checkTrustmeCounter(void);
    void buzzerTone(uint8_t buzzerType);
    
  private:
    //Internal variables
    unsigned long int debouncingTimestamp = 0;
    uint8_t trustmeCounter;
    smokeSensorType smokeSensor;
    tempHumSensorType tempSensor;
    buzzerType buzzer;
    buttonStatusType buttonStatus;
    endStopType endStop;
    relayType relay;
    supervisorType supervisor;
    lcdType lcdInternalVars;
    configModeType configMode;
    fjSensorType fjSensor;
    EEPROMPreferencesType EEPROMPreferences;
    //Functions
    buttonStatusType checkButtonStatus();
    endStopType checkEndStop();
    LiquidCrystal_I2C*  lcd;
    DHT* dht;
    void initializeLCD();
    void clearLCD();
    bool lcdNeedsUpdate();
    void lcdResetUpdate();
    void loadEEPROMPreferences();
    void storeEEPROMPreferences();    
};

#endif //  _SUPERVISOR_H_
