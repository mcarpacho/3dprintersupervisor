/******************************************************************************/
/*                                                                            */
/*   Furgalladas Schalter und Sensoren                                        */
/*                                                                            */
/*   This code is released under MIT license                                  */
/*                                                                            */
/******************************************************************************/


/** \file

   supervisorTypes.h - 3d printer supervisor project


   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> Source:   </td> <td> supervisorTypes.h                     </td></tr>
   <tr> <td> Revision: </td> <td> 1.1                              </td></tr>
   <tr> <td> Status:   </td> <td> ACCEPTED                         </td></tr>
   <tr> <td> Author:   </td> <td> Miguel Carpacho                  </td></tr>
   <tr> <td> Date:     </td> <td> 26-November-2018 22:25:56        </td></tr>
   </table>

   \n
   <table border="0" cellspacing="0" cellpadding="0">
   <tr> <td> COMPONENT: </td> <td> printerSupervisor </td></tr>
   <tr> <td> TARGET:    </td> <td> Arduino </td></tr>
   </table>
*/

/*MIT License

Copyright (c) [2018] [M.Carpacho]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
#ifndef _SUPERVISORTYPES_H_
#define _SUPERVISORTYPES_H_

//Type definitions
typedef struct fjSensorType {
  unsigned long lastFallingEdgeTimeMark;
  unsigned long lastRisingEdgeTimeMark;
  unsigned long lastLowStableStateTimeMark;
  uint8_t previousState;
  uint8_t FJSensorPin;
  //bool FJSensorActive;//in supervisor type
} fjSensor;


typedef struct smokeSensorType {
  uint16_t value;
  uint16_t threshold;
} smokeSensor;


typedef struct tempHumSensorType {
  uint8_t temperatureValue;
  uint8_t humidityValue;
  uint8_t temperatureThreshold;
  uint8_t tempSensorPin;
} tempSensor;

typedef struct buzzerType {
  uint8_t mode;
  uint8_t buzzerPin;
  uint8_t buzzerActive;
  bool buzzerFlag;
} buzzer;

typedef struct endStopType {
  uint8_t endStopState;
  uint8_t endStopPin;
} endStop;

typedef struct lcdType {
  uint8_t contrast;
  uint8_t brightness;
  uint8_t continuosMode;
  unsigned long lcdUpdateMarkTime;
} lcd;

typedef struct relayType {
  uint8_t relayStatus;
  uint8_t relayPin;
} relay;


typedef struct buttonStatusType {
  bool setButtonStatus;
  bool upButtonStatus;
  bool downButtonStatus;
  bool configButtonStatus;
  bool endStopButtonStatus;
  uint8_t setButtonPin;
  uint8_t upButtonPin;
  uint8_t downButtonPin;
  uint8_t configButtonPin;
  uint8_t endStopButtonPin;
} buttonStatus;

//Config modes variables
typedef struct configModeType{
   uint8_t currentMode;
} configMode;

//EEPROM preferences
typedef struct EEPROMPreferencesType{
  uint16_t finishedTimeout;
  uint16_t emergencyTimeout;
  uint8_t buzzerActive;
  uint8_t temperatureThreshold;
  uint16_t smokeThreshold;
  bool fjSensorActive;
}EEPROMPreferences;

typedef struct supervisorType {
  uint8_t state;
  uint8_t alarm;
  unsigned long temporizerMark;//Mark to implement temporizers
  uint16_t finishedTimeout;
  uint16_t emergencyTimeout;
  bool fjSensorActive;
  uint8_t saveEEPROM;
} supervisor;

#endif //  _SUPERVISORTYPES_H_
